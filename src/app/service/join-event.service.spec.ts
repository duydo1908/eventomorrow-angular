import { TestBed, inject } from '@angular/core/testing';

import { JoinEventService } from './join-event.service';

describe('JoinEventService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JoinEventService]
    });
  });

  it('should be created', inject([JoinEventService], (service: JoinEventService) => {
    expect(service).toBeTruthy();
  }));
});
