import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import {Observable} from 'rxjs';
import {EventDetail} from '../model/event-detail.model';

@Injectable()
export class EventDetailService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private url = '/api/events/';

  constructor(private http: Http) {
  }

  getEventDetail(idEvent: String): Observable<EventDetail> {
    const result = this.http.get(this.url + idEvent)
      .map(value => value.json())
      .catch(this.handleError);
      return result;
  }

  private handleError(error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.status);
  }
}
