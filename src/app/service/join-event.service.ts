import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

@Injectable()
export class JoinEventService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private serverUrl = 'http://192.168.78.29:8080';
  private eventsUrl = this.serverUrl + '/api/events/';
  private joinUrl = this.eventsUrl + 'quick-join';

  constructor(private http: Http) { }

  joinUs(fullName, email): Promise<string> {
    var data = {
      name: fullName,
      email: email
    };
    return this.http.post(this.joinUrl, JSON.stringify(data), {headers: this.headers})
      .toPromise()
      .then(res => res.toString() as string)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    // console.error('Ann error occurred:', error.body);
    return Promise.reject(error.message || error);
  }


}
