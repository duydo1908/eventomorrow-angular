import {Injectable, InjectableDecorator} from '@angular/core';
import {Http} from '@angular/http';
import {User} from '../model/user';
import 'rxjs/add/operator/toPromise';
import { Headers } from '@angular/http';

@Injectable()
export class UserService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private userLoginURL = 'http://localhost:9090/user/login';

  constructor(private http: Http) {}

  addNewUser(body: any): Promise<void> {
    const bodyString = JSON.stringify(body);
    return this.http.post(this.userLoginURL, JSON.stringify(body), {headers: this.headers})
      .toPromise().then(() => null).catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
