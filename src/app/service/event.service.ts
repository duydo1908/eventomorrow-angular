import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import {Event} from '../model/event';

@Injectable()
export class EventService {
  private _my_event_url = '/api/events/search?oid=';

  // HEADER: Headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) {
  }

  private handleError(error: any) {
    console.log(error);
    return Observable.throw(error.status);
  }

  getData() {
    return this.http.get('/api/events')
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  getMyEvents(id: string): Promise<Event[]> {
    const url = `${this._my_event_url}${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Event[])
      .catch(this.handleError);
  }
}
