import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EventDetailComponent} from './event-detail/event-detail.component';
import {EventDetailService} from './service/event-detail.service';
import {HomepageComponent} from './homepage/homepage.component';
import {MyEventsComponent} from './homepage/my-events/my-events.component';

const appRouter: Routes = [
  {
    path: 'events/:id',
    component: MyEventsComponent
  },
  {
    path: 'detail-event/:id',
    component: EventDetailComponent
  },
  {
    path: '',
    component: HomepageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRouter
      // , {
      //   enableTracing: true
      // }
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [
    EventDetailService
  ]
})

export class AppRoutingModule {
}
