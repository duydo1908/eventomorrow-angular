import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {AppRoutingModule} from './app.routing.module';

import {EventDetailService} from './service/event-detail.service';

import {AppComponent} from './app.component';
import {NavbarComponent} from './homepage/navbar/navbar.component';
import {FooterComponent} from './homepage/footer/footer.component';
import {CoverComponent} from './homepage/cover/cover.component';
import { JoinEventComponent } from './join-event/join-event.component';

import {UpcomingComponent} from './homepage/upcoming/upcoming.component';
import {MyEventsComponent} from './homepage/my-events/my-events.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {JoinEventService} from './service/join-event.service';
import {OwlModule} from 'ngx-owl-carousel';
import {EventService} from './service/event.service';
import {EventDetailComponent} from './event-detail/event-detail.component';
import {HomepageComponent} from './homepage/homepage.component';
import {CreateEventService} from './service/create-event.service';

import {SocialSigninComponent} from './social.signin/google-sign-in.component';
import {CreateEventComponent} from './create-event/create-event.component';

@NgModule({
  declarations: [
    AppComponent,
    SocialSigninComponent,
    AppComponent,
    NavbarComponent,
    FooterComponent,
    CoverComponent,
    UpcomingComponent,
    EventDetailComponent,
    HomepageComponent,
    JoinEventComponent,
    MyEventsComponent,
    SocialSigninComponent,
    CreateEventComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    OwlModule,
    HttpModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [AppComponent, EventService, EventDetailService, JoinEventService, CreateEventService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
