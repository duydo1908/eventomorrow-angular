import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {Event} from '../model/event';
import {CreateEventService} from '../service/create-event.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {
  event: Event;
  errorMessage: any;
  eventForm: FormGroup;

  constructor(private _fb: FormBuilder, private createEventService: CreateEventService, private route: Router) { }

  ngOnInit() {
    this.eventForm = this._fb.group({
      title: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      locationName: ['', Validators.required],
      location: ['', Validators.required]
    });
  }

  onSubmit(newEvent: NgForm, event: Event) {
    // event.preventDefault();
    const myNewEvent = newEvent.value;
    this.createEvent(myNewEvent);
    newEvent.reset();
  }

  createEvent(newEvent: any) {
    this.createEventService.createEvent(newEvent)
      .subscribe(json => {
        },
        error => {
          this.errorMessage = <any>error;
        });
  }
}
