export class Event {
  id: number;
  title: string;
  locationName: string
  location: string;
  coverImage: string;
  startTime: number;
  endTime: number
  shortDescription: string;
}
