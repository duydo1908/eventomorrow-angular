export class EventDetail {
  constructor(public id: number,
              public createdDate: number,
              public lastUpdatedDate: number,
              public status: boolean,
              public prefixCode: String,
              public title: String,
              public  location: String,
              public locationName: String,
              public coverImage: String,
              public startTime: number,
              public endTime: number,
              public description: String,
              public shortDescription: String,
              public document: String,
              public publish: boolean,
              public cancelled: boolean) {
  }
}
