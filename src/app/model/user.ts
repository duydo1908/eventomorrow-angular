export class User {
  createdDate: string;
  lastUpdatedDate: string;
  status: number;
  email: string;
  name: string;
  role: string;
}
