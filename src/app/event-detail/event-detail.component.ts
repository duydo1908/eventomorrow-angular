import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/switchMap';
import {EventDetail} from '../model/event-detail.model';
import {EventDetailService} from '../service/event-detail.service';


@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.css']
})
export class EventDetailComponent implements OnInit {

  private eventDetail: EventDetail = {
    id: 0,
    createdDate: 0,
    lastUpdatedDate: 0,
    status: false,
    prefixCode: '',
    title: '',
    location: '',
    locationName: '',
    coverImage: 'https://udemy-images.udemy.com/course/750x422/756150_c033_2.jpg',
    // coverImage: '',
    startTime: 0,
    endTime: 0,
    description: '',
    shortDescription: '',
    document: '',
    publish: false,
    cancelled: false
  };
  private statusCode: number;

  constructor(private router: ActivatedRoute,
              private eventDetailService: EventDetailService) {
  }

  ngOnInit() {
    this.router.paramMap
      .switchMap((params: ParamMap) => this.eventDetailService.getEventDetail(params.get('id')))
      .subscribe(data => {
          this.eventDetail = data;
          // console.log(data);
        },
        errorCode => this.statusCode = errorCode);
  }
}
