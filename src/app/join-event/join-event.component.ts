import {Component, OnInit, ViewChild} from '@angular/core';
import { JoinEventService } from '../service/join-event.service';

@Component({
  selector: 'app-join-event',
  templateUrl: './join-event.component.html',
  styleUrls: ['./join-event.component.css']
})
export class JoinEventComponent implements OnInit {

  tab: string = 'joinUs';
  emailMessageError: string = null;
  nameMessageError: string = null;

  error = {
    EMAIL_INVALID: 'EMAIL_INVALID',
    NAME_NULL: 'NAME_NULL',
    EMAIL_NULL: 'EMAIL_NULL'
  };

  constructor(
    private joinEventService: JoinEventService
  ) { }

  ngOnInit() {
  }

  onSelect(tab): void {
    this.tab = tab;
  }

  joinUs(fullName, email): void {
    this.emailMessageError = null;
    this.nameMessageError = null;
    fullName = fullName.trim();
    email = email.trim();
    this.joinEventService.joinUs(fullName, email).then(
      message => {
        document.getElementById('close-modal').click();
      },
      err => {
        status = err._body;
        if (status === this.error.EMAIL_INVALID) {
          this.emailMessageError = 'Your email is invalid!';
        }
        if (status === this.error.NAME_NULL) {
          this.nameMessageError = 'Your full name is required!'
        }
        if (status === this.error.EMAIL_NULL) {
          this.emailMessageError = 'Your email is required';
        }
      });
  }

}
