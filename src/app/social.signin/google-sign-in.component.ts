import {Component, NgZone} from '@angular/core';
import {UserService} from '../service/user.service';
import {User} from '../model/user';

@Component({
  selector: 'app-signin-up',
  templateUrl: 'google-sign-in.component.html',
  styleUrls: ['google-sign-in.component.css'],
  providers: [UserService]
})

export class SocialSigninComponent {
  user: User;
  constructor(ngZone: NgZone, private userService: UserService) {
    window['onSignIn'] = (user) => ngZone.run(() => this.onSignIn(user));
  }

  onSignIn(googleUser): void {
    const profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
    this.user = {
      'createdDate': '2017-01-01',
      'lastUpdatedDate': '2017-01-01',
      'status': 1,
      'email': profile.getEmail(),
      'name': profile.getName(),
      'role': 'organizer'
    };
    console.log(this.user);
    this.userService.addNewUser(this.user);
  }
}
