import {Component, OnInit} from '@angular/core';
import {EventService} from '../../service/event.service';
import {ActivatedRoute, Params} from '@angular/router';
import {Event} from '../../model/event';

@Component({
  selector: 'app-my-events',
  templateUrl: './my-events.component.html',
  styleUrls: ['./my-events.component.css']
})

export class MyEventsComponent implements OnInit {
  events: Event[];
  private organizerId: string;

  constructor(private eventService: EventService, private router: ActivatedRoute) {
  }

  getMyEvents(id: string): void {
    this.eventService
      .getMyEvents(id)
      .then(events => this.events = events);
  }

  ngOnInit() {
    this.router.params.subscribe((params: Params) => {
      this.organizerId = params['id'];
    });

    this.getMyEvents(this.organizerId);
  }
}
