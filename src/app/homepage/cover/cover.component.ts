import {Component, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-cover',
  templateUrl: './cover.component.html',
  styleUrls: ['./cover.component.css']
})
export class CoverComponent implements OnInit {
   listImg: string[] = [
    'https://hdwallsource.com/img/2014/5/high-resolution-images-21213-21750-hd-wallpapers.jpg',
    'http://www.mb3d.co.uk/mb3d/Free_Desktop,_Tablet_and_Ipad_High_res_Wallpapers_Page_3_files/High_Resolution_Destop_Wallpaper_Photo_Emats_09.jpg',
    'https://www.baleap.org/wp-content/uploads/2016/04/iStock_000075820185_Full-e1461448442412.jpg'
  ];
  constructor() {
  }

  ngOnInit() {
  }

}



