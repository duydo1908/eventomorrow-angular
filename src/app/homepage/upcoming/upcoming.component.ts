import {Component, OnInit} from '@angular/core';
import {EventService} from '../../service/event.service';
import {Event} from '../../model/event';

@Component({
  selector: 'app-upcoming',
  templateUrl: './upcoming.component.html',
  styleUrls: ['./upcoming.component.css']
})
export class UpcomingComponent implements OnInit {
  listEvent: Event[];

  constructor(private eventService: EventService) {
  }

  ngOnInit() {
    this.loadData();
  }

  private loadData() {
    this.eventService.getData()
      .subscribe(
        (data) => {
          this.listEvent = data;
          console.log(this.listEvent);
        },
        (error) => console.log(error),
      )
    ;
  }

}
