import { EventomorrowAngularPage } from './app.po';

describe('eventomorrow-angular App', () => {
  let page: EventomorrowAngularPage;

  beforeEach(() => {
    page = new EventomorrowAngularPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
